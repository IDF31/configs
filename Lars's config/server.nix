
{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      (import (builtins.fetchGit { url = "https://github.com/rycee/home-manager.git"; ref = "master";}) {}).nixos
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/vda";
  boot.supportedFilesystems = [ "zfs" ];
  services = {
    zfs.autoScrub.enable = true;
    zfs.autoSnapshot = {
      enable = true;
      frequent = 8; # keep the latest eight 15-minute snapshots (instead of four)
      monthly = 1;  # keep only one monthly snapshot (instead of twelve)
    };
    openssh.enable = true;
    quassel = {
      enable = true;
      interfaces = ["0.0.0.0"]; #this is my server. it should gather messages when i am offline
    };
    nginx = { #wiki helped here
      enable = true;
      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
      commonHttpConfig = ''
      # Add HSTS header with preloading to HTTPS requests.
      # Adding this header to HTTP requests is discouraged
      map $scheme $hsts_header {
          https   "max-age=31536000; includeSubdomains; preload";
      }
      add_header Strict-Transport-Security $hsts_header;

      # Enable CSP for your services.
      #add_header Content-Security-Policy "script-src 'self'; object-src 'none'; base-uri 'none';" always;

      # Minimize information leaked to other domains
      add_header 'Referrer-Policy' 'origin-when-cross-origin';

      # Disable embedding as a frame
      add_header X-Frame-Options DENY;

      # Prevent injection of code in other mime types (XSS Attacks)
      add_header X-Content-Type-Options nosniff;

      # Enable XSS protection of the browser.
      # May be unnecessary when CSP is configured properly (see above)
      add_header X-XSS-Protection "1; mode=block";

      # This might create errors
      proxy_cookie_path / "/; secure; HttpOnly; SameSite=strict";
    '';

    # Add any further config to match your needs, e.g.:
    virtualHosts = let
      base = locations: {
      inherit locations;

      forceSSL = true;
      enableACME = true;
    };
    proxy = port: base {
      "/".proxyPass = "http://127.0.0.1:" + toString(port) + "/";
    };
    in {
      # just 9090 from docker
      "my.domain" = proxy 9090 // { default = true; };
    };
  };
  };
  networking = {
    hostId = "notsharingthat"; #why does zfs need that
    dhcpcd.enable = false;
    firewall.allowedTCPPorts = [ 80 443 22 4242 10113];
    usePredictableInterfaceNames = false;
    hostName = "notsharingthat";
    interfaces.eth0.ipv4.addresses = [ {
      address = "notsharingthat";
      prefixLength = 22;
    } ];
    defaultGateway = "notsharingthat";
    nameservers = [ "notsharingthat"];
    localCommands = ''
      ifconfig eth0 broadcast notsharingthat
    '';
  };
  virtualisation.docker = {
    enable = true;
    storageDriver = "zfs";
    autoPrune.enable = true;
  };

  i18n = {
     consoleFont = "Lat2-Terminus16";
     consoleKeyMap = "de";
     defaultLocale = "en_US.UTF-8";
   };

  time.timeZone = "Europe/Berlin";

   environment.systemPackages = with pkgs; [
     wget vimHugeX git docker-compose
   ];

   users.users.lars = {
     isNormalUser = true;
     extraGroups = [ "wheel" ]; 
   };
   systemd.services.docker-compose = {
    enable = true;
    description = "A service for my docker-compose file. Nextcloud on NixOS is not fun";
    after = [ "network-online.target" "docker.service" ];
    serviceConfig = {
	  WorkingDirectory = "/home/lars";
	  ExecStartPre = ''/run/current-system/sw/bin/docker-compose down -v ; /run/current-system/sw/bin/docker-compose rm -fv'';
          ExecStart = ''/run/current-system/sw/bin/docker-compose up'';
	  ExecStop = ''/run/current-system/sw/bin/docker-compose down -v'';
    };
    wantedBy = [ "multi-user.target" ];
  };
  home-manager.users.lars =
    let
      master = import (builtins.fetchGit { url = https://github.com/NixOS/nixpkgs.git; ref = "master";}) {};#this is kinda cool, hm?
      stable = import (builtins.fetchGit { url = https://github.com/NixOS/nixpkgs.git; ref = "release-18.09";}) {};#ill leave that here if i need it
    in {
      programs.zsh.enable = true;
      programs.zsh.history.size = 1000000;
      programs.zsh.oh-my-zsh.enable = true; #i kinda want zim zsh here but i am too lazy to do that
      programs.zsh.oh-my-zsh.plugins = ["history" "git" "cp" "systemd" "colorize" "command-not-found"];
      programs.zsh.plugins = [
        {
        name = "zsh-autosuggestions";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-autosuggestions";
          rev = "v0.5.0";
          sha256 = "19qkg4b2flvnp2l0cbkl4qbrnl8d3lym2mmh1mx9nmjd7b81r3pf";
          };
        }
        {
        name = "zsh-syntax-highlighting";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-syntax-highlighting";
          rev = "v0.6.0";
          sha256 = "0zmq66dzasmr5pwribyh4kbkk23jxbpdw4rjxx0i7dx8jjp2lzl4";
          };
        }
      ];
      programs.zsh.oh-my-zsh.theme = "agnoster";#first one that is not completely ugly
      programs.zsh.initExtra = "zstyle \":completion:*:commands\" rehash 1 "; #autorehash and sourcing .profile makes sense
      home.file."docker-compose.yaml".text = ''
version: '3'
services:
  nextcloud:
    image: nextcloud
    container_name: nextcloud
    volumes:
      - ./nextcloud/nextcloud:/var/www/html
      - ./nextcloud/apps:/var/www/html/custom_apps
      - ./nextcloud/config:/var/www/html/config
      - ./nextcloud/data:/var/www/html/data
    restart: always
    depends_on:
      - nextclouddb
    ports: 
      - "9090:80"

  nextclouddb:
    image: mariadb
    container_name: nextclouddb
    command: --transaction-isolation=READ-COMMITTED --binlog-format=ROW
    restart: always
    volumes:
      - ./nextclouddb:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=notsharingthat
      - MYSQL_PASSWORD=notsharingthat
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud
      '';
    };# so i will just host nextcloud over docker. that is a lot more comfortable than over nixos
  system.stateVersion = "19.03"; 

}
