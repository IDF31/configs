{ config, pkgs, lib, ... }:
{
  /*
  To setup my / with encrypted zfs, i do this
  DISK=/dev/disk/by-id/ata-Samsung_SSD_860_EVO_1TB_S3Z9NB0KB53196K
  sgdisk -a1 -n2:34:2047 -t2:EF02 $DISK
  sgdisk -n3:1M:+512M -t3:EF00 $DISK
  sgdisk -n1:0:0 -t1:BF01 $DISK
  zpool create -O mountpoint=none -R /mnt -o feature@encryption=enabled zroot $DISK-part1
  zfs create -o  acltype=posixacl -o xattr=sa -o encryption=aes-256-gcm -o keyformat=passphrase -o mountpoint=none -o compression=lz4 -o dedup=verify zroot/root
  zfs create -o mountpoint=legacy -o sync=disabled zroot/root/tmp
  zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=true zroot/root/home
  zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=true zroot/root/nixos
  mount -t zfs zroot/root/nixos /mnt
  mkdir /mnt/{home,tmp,boot}
  mkfs.vfat $DISK-part3
  mount $DISK-part3 /mnt/boot

  Look at the boot = { section closely.
  If you fuck up, you remount via 
  DISK=/dev/disk/by-id/ata-Samsung_SSD_860_EVO_1TB_S3Z9NB0KB53196K
  mount $DISK-part3 /mnt/boot
  zpool import -a
  zfs load-key -a
  mount -t zfs zroot/root/nixos /mnt


  A cached HDD will happen in the future
  */
  imports =
    [
    /etc/nixos/hardware-configuration.nix
    (import (builtins.fetchGit { url = "https://github.com/rycee/home-manager.git"; ref = "master";}) {}).nixos
  ];
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntelhy = pkgs.vaapiIntel.override {
      enableHybridCodec = true;
    };
  };
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.pulseaudio = true; #only mumble needs that
  nixpkgs.config.android_sdk.accept_license = true; #why the fuck is that a config?
  boot = {
    zfs = {
      enableUnstable = true;
      requestEncryptionCredentials = true;
    };
    consoleLogLevel = 2;
    tmpOnTmpfs = true; #tmp should be a tmpfs. thanks
    initrd.kernelModules = [ "nouveau" "i915" "vfio-pci" ]; #lets try to not break sddm or libvirt
    kernelPackages = pkgs.linuxPackages_latest;
    supportedFilesystems = [ "ntfs" "btrfs" "zfs" ]; #idk, i can write to ntfs now
    extraModprobeConfig =  ''
      options thinkpad_acpi fan_control=1
      options thinkpad_acpi experimental=1
      options i915 fastboot=1
      options i915 enable_fbc=1
      options i915 lvds_downclock=1
      options i915 modeset=0
    '';
    loader = {
      grub = {
        enable = true;
        version = 2;
        device = "/dev/disk/by-id/ata-Samsung_SSD_860_EVO_1TB_S3Z9NB0KB53196K";
        efiSupport = true;
        efiInstallAsRemovable = true; #lets make our system bootable after a BIOS reset
        copyKernels = true;
        zfsSupport = true;
      };
    };
    kernelModules = [ "acpi_call" "tp-smapi" "kvm-intel" ]; #thanks novideo
    extraModulePackages = [
      config.boot.kernelPackages.tp_smapi 
      config.boot.kernelPackages.acpi_call];
      kernelParams = [ "intel_iommu=on" "net.ifnames=0" "biosdevname=0" "acpi_osi=\"!Windows 2012\"" "nmi_watchdog=0" 
                      "snd-hda-intel.power_save=1" "iwlwifi.led_mode=2" "iwlwifi.power_save=Y" "iwlwifi.11n_disable=1" 
                      "e1000e.SmartPowerDownEnable=1" "quiet" "vga=current" "systemd.show_status=0"]; #make it stop
  };
  virtualisation = {
    libvirtd =  {
      enable = true;
      extraConfig = ''
            unix_sock_group = "libvirtd"
            nix_sock_rw_perms = "0770"''; #why is that not default. is it default? don't care
      qemuOvmf = true;
      qemuVerbatimConfig = ''user = "1000" ''; #audio
    };
  };
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "de"; #reee bratwurst
    defaultLocale = "en_US.UTF-8";
  };
  time.timeZone = "Europe/Berlin";
  environment.systemPackages = with pkgs;  [
    gnome3.dconf #virtmanager shits itself otherwise
    acpi
    tpacpi-bat #when was the last time i recalibrated my battery?
    zsh-completions
    nix-zsh-completions #still wondering if they actually work...
    mumble_git
    libsForQt511.qtstyleplugin-kvantum #BLUR
  ];
  networking = {
    hostName = "NixLarf";
    hostId = "8423e348";
    networkmanager.enable = true;
    firewall.enable = false;
  };
  hardware = {
    cpu.intel.updateMicrocode = true;#super fast botnet upgrades
    opengl = {
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        vaapiIntelhy
        vaapiVdpau
        libvdpau-va-gl
        intel-media-driver #accelerated video is a mystery
        libva-full
        ];
    };
    pulseaudio.enable = true; #hewwo lennart!
    pulseaudio.support32Bit = true;
    bluetooth.enable = true;
  };
  sound.enable = true; # why alsa again?
  sound.mediaKeys.enable = true;
  services = {
    tlp = {
      enable = true;
         extraConfig = ''
            SATA_LINKPWR_ON_AC=max_performance
            SATA_LINKPWR_ON_BAT=min_power
            WIFI_PWR_ON_AC=off
            WIFI_PWR_ON_BAT=on
            CPU_SCALING_GOVERNOR_ON_AC=powersave
            CPU_SCALING_GOVERNOR_ON_BAT=powersave
            DEVICES_TO_ENABLE_ON_STARTUP="wifi"
            DEVICES_TO_DISABLE_ON_STARTUP="bluetooth wwan"
            RESTORE_DEVICE_STATE_ON_STARTUP=0
            RESTORE_THRESHOLDS_ON_BAT=0
            DISK_DEVICES="ata-ST2000LM015-2E8174_WDZ4QM43"
            DISK_APM_LEVEL_ON_AC="192"
            DISK_APM_LEVEL_ON_BAT="1"
            DISK_SPINDOWN_TIMEOUT_ON_AC="36"
            DISK_SPINDOWN_TIMEOUT_ON_BAT="18"
            USB_AUTOSUSPEND=1
        '';
      };
    earlyoom = {
      enable = true;
      freeMemThreshold = 2;
    };
    flatpak.enable = true; #AH MAKE IT STOP
    fstrim.enable = true; #i should visit the barber some time
    thermald.enable = true; #liquid metal is not enough
    printing.enable = true; #i don't even own a printer
    colord.enable = true; #thats gay
    samba.enable = true; #dancin. i don't even have Windows running here
    xserver = {
      enable =true;
      layout = "de";
      videoDrivers = [
        "intel"
        "nouveau"
        "modesetting"
      ];
      libinput.enable = true; #why is that an extra option? 
       displayManager.sessionCommands = '' 
        xrandr --setprovideroutputsource 1 0
        xmodmap /home/lars/.Xmodmap
        ln -sf /home/lars/.nix-profile/share/applications/org.keepassxc.KeePassXC.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/flameshot.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/nextcloud.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/telegram-desktop.desktop /home/lars/.config/autostart/.
        ''; #the "aut" in autostart stands for autistic. also fuck you nvidia. The xrandr command makes displays connected to the nvidia (on my W530 all external ones) available to output to.
      displayManager.sddm.enable = true;
      desktopManager = {
        xterm.enable = false;
        plasma5 = { #show me a decent alternative, kthxbye
          enable = true;
          enableQt4Support = true; #Who uses qt4 anymore?
        };
      };
    };
  };
  systemd.services.guix-daemon = { #Guix daemon for extra python compiling
    enable = true;
    description = "Build daemon for GNU Guix";
    serviceConfig = {
      ExecStart = "/var/guix/profiles/per-user/root/current-guix/bin/guix-daemon --build-users-group=guixbuild";
      Environment="GUIX_LOCPATH=/root/.guix-profile/lib/locale";
      RemainAfterExit="yes";
      StandardOutput="syslog";
      StandardError="syslog";
      TaskMax= "8192";
    };
    wantedBy = [ "multi-user.target" ];
  };
  systemd.services.autoGc = {
    serviceConfig = {
      ExecStart = "${config.nix.package.out}/bin/nix-collect-garbage --delete-older-than 14d";
    };
  };
  systemd.timers.autoGc = { #lets clean up and auto-hardlink at 2 am
    enable = true;
    timerConfig = {
      Unit = "autoGc.service";
      OnCalendar = "02:00";
      Persistent = "true";
    };
    wantedBy = [ "timers.target" ];
  };
  systemd.services.autoUpgrade = {
    after = [ "network-online.target" "autoGc.service" ];
    path = [ pkgs.gnutar pkgs.xz.bin config.nix.package.out ];
    environment = config.nix.envVars //
    { inherit (config.environment.sessionVariables) NIX_PATH;
    HOME = "/root";
  } // config.networking.proxy.envVars; #nix sucks

    serviceConfig = {
      ExecStart = "${config.system.build.nixos-rebuild}/bin/nixos-rebuild switch --upgrade";
    };
    wantedBy= [ "autoGc.service" "autoUpgrade.service" ];
  };
  systemd.services.autoOptimise = {
    after = [ "network-online.target" ];
    serviceConfig = {
      ExecStart = "${config.nix.package.out}/bin/nix-store --optimise";
    };
    wantedBy = [ "autoUpgrade.service" ];
  };
  systemd.extraConfig = "DefaultLimitNOFILE=1048576"; #thanks lennart, please stop crashing my system because i have too many open files
  programs.adb.enable = true;
  users.extraUsers = {
    lars = {
      isNormalUser = true;
      uid = 1000;
      home = "/home/lars";
      extraGroups = [ "wheel" "networkmanager" "libvirtd" "video" "adbusers" "kvm" ];
      shell = pkgs.zsh;
    };
    guixbuilder1 = { #i will write a function for that... next year
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder2 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder3 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder4 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder5 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder6 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder7 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    guixbuilder8 = {
      group = "guixbuild";
      home = "/var/empty";
      shell = pkgs.nologin;
      isSystemUser = true; 
    };
    };
  environment.pathsToLink = [ "/share/zsh" ]; #systemd-autocomplete uwu
  nix.nixPath = ["nixpkgs=http://nixos.org/channels/nixos-19.03/nixexprs.tar.xz" "nixos-config=/home/lars/configs/Lars\'s\ config/configuration.nix" ]; #i moved my config to my home, cool hm?
  home-manager.users.lars =
    let
      unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) { }; #scrap the master plan. system on nixos-stable, rest pretty much stable
    in {
      programs.zsh.enable = true;
      programs.zsh.history.size = 1000000;
      programs.zsh.shellAliases = {
        nup = "env NIXPKGS_ALLOW_UNFREE=1 sudo -E nixos-rebuild switch --upgrade && rm ~/.cache/ksycoca5_* -rf && kbuildsycoca5 && kbuildsycoca5 --noincremental";
        nupc = "sudo nix-collect-garbage -d && sudo nix-store --optimise && env NIXPKGS_ALLOW_UNFREE=1 sudo -E nixos-rebuild switch --upgrade && rm ~/.cache/ksycoca5_* -rf && kbuildsycoca5 && kbuildsycoca5 --noincremental";
      }; # what the fuck is that? nix-store --optimise takes a long time on large stores. Though hardlinking can improve a lot sometimes... might as well use it "sometimes"
      programs.zsh.initExtra = ''zstyle ':completion:*' rehash true
      source ~/.profile
      if [[ -d "$HOME/.zim" ]]; then
      else
        git clone --recursive https://github.com/zimfw/zimfw.git $HOME/.zim
        for template_file in $HOME/.zim/templates/*; do
          user_file="$HOME/.''${template_file:t}"
          cat ''${template_file} ''${user_file}(.N) > ''${user_file}.tmp && mv ''${user_file}{.tmp,}
        done
      fi
      export ZIM_HOME=$HOME/.zim
      [[ -s ''${ZIM_HOME}/init.zsh ]] && source ''${ZIM_HOME}/init.zsh
      ''; #autorehash and sourcing .profile makes sense. Installing zim zsh because me likey
      nixpkgs.config.allowUnfree = true; #forgive me stallman for i have sinned. x11vnc sucks though
      home.file.".Xmodmap".text = ''
          remove Lock = Caps_Lock
          keysym Caps_Lock = Escape
          '';#who needs caps
      home.packages = with pkgs; [
        #(unstable.wine.override { wineBuild = "wineWow"; })
        #(unstable.winetricks.override { wine = wineWowPackages.stable; })
        #(unstable.chromium.override {useVaapi = true; })
        unstable.chromium
        unstable.anydesk
        unstable.arc-kde-theme
        unstable.ark
        unstable.cmatrix
        unstable.evince
        unstable.file
        unstable.filezilla
        unstable.ffmpeg-full
        unstable.flameshot #is gud
        unstable.gimp-with-plugins
        unstable.git
        unstable.glibcLocales
        unstable.glxinfo
        unstable.htop
        unstable.jdk
        unstable.kdeApplications.gwenview
        unstable.keepassxc
        unstable.krename-qt5
        unstable.libreoffice
        unstable.lm_sensors
        unstable.lolcat
        unstable.mpv
        unstable.ncdu
        unstable.nextcloud-client
        unstable.nix-index #i can search for files in nix packages... coool
        unstable.notepadqq #hurrdurr emacs masterrace
        unstable.oxygen #pretty hard to breathe without
        unstable.oxygen-icons5
        unstable.pavucontrol
        unstable.pciutils #why the fuck is lspci included in here but not in base?
        unstable.pcsx2 #wanna heat up my notebook?
        unstable.powertop
        unstable.psensor
        unstable.qbittorrent
        unstable.qsyncthingtray
        unstable.quasselClient
        unstable.qownnotes
        unstable.quaternion #qool matrix client
        unstable.quota
        unstable.smplayer
        unstable.syncthing
        unstable.tdesktop
        unstable.thunderbird
        unstable.tigervnc
        unstable.toilet
        unstable.unrar
        unstable.unzip
        unstable.vimHugeX
        unstable.vlc
        unstable.vscodium #powershell indent crashes on windows
        unstable.weechat
        unstable.x11vnc
        unstable.xorg.xev #pressing keys n fun
        unstable.xorg.xmodmap
        unstable.xrdp
        unstable.yakuake #i should configure it already
        unstable.youtube-dl
      ];
  };
powerManagement.enable = true;
powerManagement.powertop.enable = true;
system.copySystemConfiguration = true;
system.stateVersion = "18.09";
}
