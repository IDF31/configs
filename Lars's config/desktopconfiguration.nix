{ config, pkgs, lib, ... }:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
  master = import (builtins.fetchGit { url = "https://github.com/NixOS/nixpkgs.git"; ref = "master";}) {};
in 
{
  /*
  To setup my / with encrypted zfs, i do this
  DISK=/dev/disk/by-id/ata-Samsung_SSD_860_EVO_250GB_S3YJNB0K155143Y
  sgdisk -a1 -n2:34:2047 -t2:EF02 $DISK
  sgdisk -n3:1M:+512M -t3:EF00 $DISK
  sgdisk -n1:0:0 -t1:BF01 $DISK
  zpool create -O mountpoint=none -R /mnt -o feature@encryption=enabled zroot $DISK-part1
  zfs create -o mountpoint=none -o compression=lz4 -o dedup=verify zroot/root
  zfs create -o mountpoint=legacy -o sync=disabled zroot/root/tmp
  zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=true zroot/root/home
  zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=true zroot/root/nixos
  mount -t zfs zroot/root/nixos /mnt
  mkdir /mnt/{home,tmp,boot}
  mkfs.vfat $DISK-part3
  mount $DISK-part3 /mnt/boot

  Look at the boot = { section closely.
  If you fuck up, you remount via 
  DISK=/dev/disk/by-id/ata-Samsung_SSD_860_EVO_250GB_S3YJNB0K155143Y
  mount $DISK-part3 /mnt/boot
  zpool import -a
  zfs load-key -a
  mount -t zfs zroot/root/nixos /mnt


  My storage for Downloads, Nextcloud, Steam, other random games and so on...
  zpool create -O mountpoint=none -R /mnt -f tank raidz /dev/sda /dev/sdc /dev/sdd /dev/sde
  zfs create -o mountpoint=none -o compression=lz4 -o dedup=verify tank/storage
  zfs create -o mountpoint=legacy tank/storage/steam
  zfs create -o mountpoint=legacy tank/storage/Videos 
  mount -t zfs tank/storage/Games /home/lars/Games 
  you get the idea


  for nixos-unstable to work you need to do:
  nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable
  nix-channel --update
  as root. 
  */
  imports =
    [
    /etc/nixos/hardware-configuration.nix
    (import (builtins.fetchGit { url = "https://github.com/rycee/home-manager.git"; ref = "master";}) {}).nixos
  ];
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.pulseaudio = true; #only mumble needs that
  nixpkgs.config.android_sdk.accept_license = true; #why the fuck is that a config?
  boot = {
    zfs.enableUnstable = true;
    consoleLogLevel = 2;
    tmpOnTmpfs = true; #tmp should be a tmpfs. thanks
    initrd.kernelModules = [ "nouveau" "i915" "vfio-pci" ]; #lets try to not break sddm or libvirt
    kernelPackages = pkgs.linuxPackages_latest;
    supportedFilesystems = [ "ntfs" "btrfs" "zfs" ]; #idk, i can write to ntfs now
    extraModprobeConfig =  ''
      options thinkpad_acpi fan_control=1
      options thinkpad_acpi experimental=1
      options i915 fastboot=1
      options i915 enable_fbc=1
      options i915 lvds_downclock=1
      options i915 modeset=0
    '';
    loader = {
      grub = {
        enable = true;
        version = 2;
        device = "/dev/disk/by-id/ata-Samsung_SSD_860_EVO_250GB_S3YJNB0K155143Y";
        zfsSupport = true;
      };
    };
    kernelModules = [ "acpi_call" "tp-smapi" "kvm-intel" ]; #thanks novideo
    kernelParams = [ "intel_iommu=on" "net.ifnames=0" "biosdevname=0" "zfs.zfs_arc_max=1073741274" ]; #make it stop
  };
  virtualisation = {
    libvirtd =  {
      enable = true;
      extraConfig = ''
            unix_sock_group = "libvirtd"
            nix_sock_rw_perms = "0770"''; #why is that not default. is it default? don't care
      qemuOvmf = true;
      qemuVerbatimConfig = ''user = "1000" ''; #audio
    };
  };
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "de"; #reee bratwurst
    defaultLocale = "en_US.UTF-8";
  };
  time.timeZone = "Europe/Berlin";
  environment.systemPackages = with pkgs;  [
    gnome3.dconf #virtmanager shits itself otherwise
    mumble_git
    libsForQt511.qtstyleplugin-kvantum #BLUR
  ];
  networking = {
    hostName = "NixLarf";
    hostId = "8423e348";
    networkmanager.enable = true;
    firewall.enable = false;
  };
  hardware = {
    cpu.intel.updateMicrocode = true;#super fast botnet upgrades
    opengl.driSupport32Bit = true;
    opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ vaapiVdpau xorg.xf86videoamdgpu ] ;
    pulseaudio.enable = true; #hewwo lennart!
    pulseaudio.support32Bit = true;
    bluetooth.enable = true;
  };
  sound.enable = true; # why alsa again?
  sound.mediaKeys.enable = true;
  services = {
    zfs.autoScrub.enable = true;
    zfs.autoSnapshot = {
      enable = true;
      frequent = 8; # keep the latest eight 15-minute snapshots (instead of four)
      monthly = 1;  # keep only one monthly snapshot (instead of twelve)
    };
    earlyoom = {
      enable = true;
      freeMemThreshold = 2;
    };
    flatpak = {
      enable = true; #AH MAKE IT STOP
      extraPortals = [ pkgs.plasma5.xdg-desktop-portal-kde ];
    };
    samba.enable = true; #dancin. i don't even have Windows running here
    xserver = {
      enable =true;
      layout = "de";
      videoDrivers = [
        "amdgpu"
      ];
      libinput.enable = true; #why is that an extra option? 
       displayManager.sessionCommands = '' 
        xmodmap /home/lars/.Xmodmap
        ln -sf /home/lars/.nix-profile/share/applications/org.keepassxc.KeePassXC.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/flameshot.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/nextcloud.desktop /home/lars/.config/autostart/.
        ln -sf /home/lars/.nix-profile/share/applications/telegram-desktop.desktop /home/lars/.config/autostart/.
        ''; #the "aut" in autostart stands for autistic. also fuck you nvidia. The xrandr command makes displays connected to the nvidia (on my W530 all external ones) available to output to.
      displayManager.sddm.enable = true;
      desktopManager = {
        xterm.enable = false;
        plasma5 = { #show me a decent alternative, kthxbye
          enable = true;
          enableQt4Support = true; #Who uses qt4 anymore?
        };
      };
    };
  };
  systemd.services.autoGc = {
    serviceConfig = {
      ExecStart = "${config.nix.package.out}/bin/nix-collect-garbage --delete-older-than 14d";
    };
  };
  systemd.timers.autoGc = { #lets clean up and auto-hardlink at 2 am
    enable = true;
    timerConfig = {
      Unit = "autoGc.service";
      OnCalendar = "02:00";
      Persistent = "true";
    };
    wantedBy = [ "timers.target" ];
  };
  systemd.services.autoUpgrade = {
    after = [ "network-online.target" "autoGc.service" ];
    path = [ pkgs.gnutar pkgs.xz.bin config.nix.package.out ];
    environment = config.nix.envVars //
    { inherit (config.environment.sessionVariables) NIX_PATH;
    HOME = "/root";
  } // config.networking.proxy.envVars; #nix sucks

    serviceConfig = {
      ExecStart = "${config.system.build.nixos-rebuild}/bin/nixos-rebuild switch --upgrade";
    };
    wantedBy= [ "autoGc.service" "autoUpgrade.service" ];
  };
  systemd.services.autoOptimise = {
    after = [ "network-online.target" ];
    serviceConfig = {
      ExecStart = "${config.nix.package.out}/bin/nix-store --optimise";
    };
    wantedBy = [ "autoUpgrade.service" ];
  };
  systemd.extraConfig = "DefaultLimitNOFILE=1048576"; #thanks lennart, please stop crashing my system because i have too many open files
  programs.adb.enable = true;
  users.extraUsers = {
    lars = {
      isNormalUser = true;
      uid = 1000;
      home = "/home/lars";
      extraGroups = [ "wheel" "networkmanager" "libvirtd" "video" "adbusers" "kvm" ];
      shell = pkgs.zsh;
    };
  };
  environment.pathsToLink = [ "/share/zsh" ]; #systemd-autocomplete uwu
  nix.nixPath = ["nixpkgs=http://nixos.org/channels/nixos-19.03/nixexprs.tar.xz" "nixos-config=/home/lars/configs/Lars\'s\ config/desktopconfiguration.nix" ]; #i moved my config to my home, cool hm?
  home-manager.users.lars = {
      programs.zsh.enable = true;
      programs.zsh.history.size = 1000000;
      programs.zsh.shellAliases = {
        nup = "env NIXPKGS_ALLOW_UNFREE=1 sudo -E nixos-rebuild switch --upgrade && rm ~/.cache/ksycoca5_* -rf && kbuildsycoca5 && kbuildsycoca5 --noincremental";
        nupc = "sudo nix-collect-garbage -d && sudo nix-store --optimise && env NIXPKGS_ALLOW_UNFREE=1 sudo -E nixos-rebuild switch --upgrade && rm ~/.cache/ksycoca5_* -rf && kbuildsycoca5 && kbuildsycoca5 --noincremental";
      }; # what the fuck is that? nix-store --optimise takes a long time on large stores. Though hardlinking can improve a lot sometimes... might as well use it "sometimes"
      programs.zsh.initExtra = ''zstyle ':completion:*' rehash true
      if [[ -d "$HOME/.zim" ]]; then
      else
        git clone --recursive https://github.com/zimfw/zimfw.git $HOME/.zim
        for template_file in $HOME/.zim/templates/*; do
          user_file="$HOME/.''${template_file:t}"
          cat ''${template_file} ''${user_file}(.N) > ''${user_file}.tmp && mv ''${user_file}{.tmp,}
        done
      fi
      export ZIM_HOME=$HOME/.zim
      [[ -s ''${ZIM_HOME}/init.zsh ]] && source ''${ZIM_HOME}/init.zsh
      ''; #autorehash and sourcing .profile makes sense. Installing zim zsh because me likey
      nixpkgs.config.allowUnfree = true; #forgive me stallman for i have sinned. x11vnc sucks though
      home.file.".Xmodmap".text = ''
          remove Lock = Caps_Lock
          keysym Caps_Lock = Escape
          '';#who needs caps
      home.packages = with pkgs; [
        master.tdesktop
        (winetricks.override { wine = wineWowPackages.full; })
        wineWowPackages.full
        unstable.chromium
        unstable.arc-kde-theme
        unstable.ark
        unstable.cmatrix
        unstable.evince
        unstable.file
        unstable.filezilla
        unstable.ffmpeg-full
        unstable.flameshot #is gud
        unstable.gimp
        unstable.git
        unstable.glibcLocales
        unstable.glxinfo
        unstable.htop
        unstable.jdk
        unstable.kdeApplications.gwenview
        unstable.keepassxc
        unstable.krename-qt5
        unstable.libreoffice
        unstable.lm_sensors
        unstable.lolcat
        unstable.mpv
        unstable.ncdu
        unstable.nextcloud-client
        unstable.nix-index #i can search for files in nix packages... coool
        unstable.notepadqq #hurrdurr emacs masterrace
        unstable.oxygen #pretty hard to breathe without
        unstable.oxygen-icons5
        unstable.pavucontrol
        unstable.pciutils #why the fuck is lspci included in here but not in base?
        unstable.pcsx2 #wanna heat up my notebook?
        unstable.powertop
        unstable.qbittorrent
        unstable.qsyncthingtray
        unstable.quasselClient
        unstable.qownnotes
        unstable.quaternion #qool matrix client
        unstable.quota
        unstable.smplayer
        unstable.steam
        unstable.syncthing
        unstable.thunderbird
        unstable.tigervnc
        unstable.toilet
        unstable.unrar
        unstable.unzip
        unstable.vimHugeX
        unstable.vlc
        unstable.vscodium #powershell indent crashes on windows
        unstable.weechat
        unstable.x11vnc
        unstable.xorg.xev #pressing keys n fun
        unstable.xorg.xmodmap
        unstable.xrdp
        unstable.yakuake #i should configure it already
        unstable.youtube-dl
      ];
    };
    system.copySystemConfiguration = true;
    system.stateVersion = "18.09";
}

